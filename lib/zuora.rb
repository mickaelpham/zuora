# frozen_string_literal: true

require 'forwardable'
require 'json'
require 'faraday'
require 'faraday_middleware'
require 'zeitwerk'

loader = Zeitwerk::Loader.for_gem
loader.setup

Faraday::Request.register_middleware bearer: Zuora::Middleware::Bearer

module Zuora
  class Error < StandardError; end

  def self.default_client
    @default_client ||= Client.new(Config.default)
  end
end
