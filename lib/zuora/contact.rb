# frozen_string_literal: true

module Zuora
  class Contact
    include Inspectable

    def self.find(id, client: Zuora.default_client)
      response = client.get("/v1/object/contact/#{id}")
      return unless response.success?

      new(attributes: response.body, client: client)
    end

    def initialize(attributes: {}, client: Zuora.default_client)
      @attributes = attributes
      @client = client
    end

    attr_reader :attributes, :client

    def id
      attributes['Id']
    end

    def email
      attributes['WorkEmail']
    end

    def inspect
      "#<#{self.class.name} id=\"#{id}\" email=\"#{email}\">"
    end
  end
end
