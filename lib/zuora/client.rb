# frozen_string_literal: true

module Zuora
  class Client
    extend Forwardable
    def_delegators :connection, :get, :post, :put, :patch, :delete

    def initialize(config)
      @config = config
    end

    def query(zoql, limit: 2_000)
      payload = {
        'conf' => { 'batchSize' => limit },
        'queryString' => zoql
      }

      post('/v1/action/query', payload).body
    end

    private

    attr_reader :config

    def_delegators :config, :api_fqdn

    def connection
      @connection ||= Faraday.new(url: base_url) do |faraday|
        faraday.request :json
        faraday.request :bearer, config

        faraday.response :json, content_type: /\bjson$/
        # faraday.response :logger
      end
    end

    def base_url
      URI("https://#{api_fqdn}")
    end
  end
end
