# frozen_string_literal: true

module Zuora
  class Product
    include Inspectable

    def self.find(id, client: Zuora.default_client)
      response = client.get("/v1/object/product/#{id}")
      return unless response.success?

      new(attributes: response.body, client: client)
    end

    def initialize(attributes: {}, client: Zuora.default_client)
      @attributes = attributes
      @client = client
    end

    attr_reader :attributes, :client

    def id
      attributes['Id']
    end

    def name
      attributes['Name']
    end

    def plans
      @plans ||= CollectionProxy.new(parent: self, foreign_key: 'ProductId', klass: CatalogPlan)
    end
  end
end
