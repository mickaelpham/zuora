# frozen_string_literal: true

module Zuora
  class CatalogPlan
    include Inspectable

    def self.find(id, client: Zuora.default_client)
      response = client.get("/v1/object/product-rate-plan/#{id}")
      return unless response.success?

      new(attributes: response.body, client: client)
    end

    def self.entity_name
      'ProductRatePlan'
    end

    def initialize(attributes: {}, client: Zuora.default_client)
      @attributes = attributes
      @client = client
    end

    attr_reader :attributes, :client

    def id
      attributes['Id']
    end

    def name
      attributes['Name']
    end
  end
end
