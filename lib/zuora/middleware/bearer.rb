# frozen_string_literal: true

module Zuora
  module Middleware
    class Bearer
      extend Forwardable

      def initialize(app, config)
        @app = app
        @config = config
      end

      def call(env)
        env.request_headers['Authorization'] = "Bearer #{token}"

        app.call(env)
      end

      private

      attr_reader :app, :config

      def_delegators :config, :api_fqdn, :oauth_user, :oauth_secret

      def token
        @token ||= begin
          response = Faraday.post(endpoint, credentials)
          raise response unless response.success?

          JSON.parse(response.body).fetch('access_token')
        end
      end

      def credentials
        {
          client_id: oauth_user,
          client_secret: oauth_secret,
          grant_type: 'client_credentials'
        }
      end

      def endpoint
        URI("https://#{api_fqdn}/oauth/token")
      end
    end
  end
end
