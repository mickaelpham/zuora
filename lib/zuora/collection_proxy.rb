# frozen_string_literal: true

module Zuora
  class CollectionProxy
    extend Forwardable

    attr_reader :parent, :klass, :foreign_key

    def_delegators :records, :size, :first, :last, :[], :each, :map, :detect, :select, :reject

    def_delegators :parent, :client

    def initialize(parent:, foreign_key:, klass:)
      @parent = parent
      @klass = klass
      @foreign_key = foreign_key
    end

    # This method queries Zuora for each record ID. It is not optimal
    # but until Zuora provides a simpler way to fetch `has_many` relationships
    # then this will have to do.
    #
    # As an alternative, `IronBank` goes the long way to query all fields
    # using ZOQL, but it requires a lot of upfront configuration.
    def records
      @records ||= record_ids.map { |id| klass.find(id, client: client) }
    end

    def inspect
      "#<#{self.class.name} klass=#{klass}>"
    end

    private

    def zoql
      "select Id from #{klass.entity_name} where #{foreign_key}='#{parent.id}'"
    end

    def record_ids
      client.query(zoql).fetch('records').flat_map(&:values)
    end
  end
end
