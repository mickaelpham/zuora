# frozen_string_literal: true

module Zuora
  module Inspectable
    def self.included(klass)
      klass.extend(ClassMethods)
    end

    module ClassMethods
      def entity_name
        name.split('::').last
      end
    end

    def inspect
      "#<#{self.class.name} id=\"#{id}\" name=\"#{name}\">"
    end
  end
end
