# frozen_string_literal: true

module Zuora
  class Config
    attr_accessor :api_fqdn, :oauth_user, :oauth_secret

    def self.default
      new.tap do |conf|
        conf.api_fqdn = ENV['ZUORA_API_FQDN']
        conf.oauth_user = ENV['ZUORA_OAUTH_USER']
        conf.oauth_secret = ENV['ZUORA_OAUTH_SECRET']
      end
    end
  end
end
