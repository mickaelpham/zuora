# frozen_string_literal: true

module Zuora
  class Account
    include Inspectable

    def self.find(id, client: Zuora.default_client)
      response = client.get("/v1/object/account/#{id}")
      return unless response.success?

      new(attributes: response.body, client: client)
    end

    def initialize(attributes: {}, client: Zuora.default_client)
      @attributes = attributes
      @client = client
    end

    attr_reader :attributes, :client

    def id
      attributes['Id']
    end

    def name
      attributes['Name']
    end

    def currency
      attributes['Currency']
    end

    def bill_to
      contacts.detect { |c| c.id == attributes['BillToId'] }
    end

    def sold_to
      contacts.detect { |c| c.id == attributes['SoldToId'] }
    end

    def contacts
      @contacts ||= CollectionProxy.new(parent: self, foreign_key: 'AccountId', klass: Contact)
    end

    def inspect
      "#<#{self.class.name} id=\"#{id}\" currency=\"#{currency}\" name=\"#{name}\">"
    end
  end
end
