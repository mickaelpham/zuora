# Zuora

Yet another Ruby client for [Zuora](https://www.zuora.com/)

## CLI Usage

Clone the repository and install the dependencies

```sh
git clone https://gitlab.com/mickaelpham/zuora.git
cd zuora
bin/setup
```

Add your [credentials][create-api-user] in `.env` and start a console

```sh
bin/console
```

Use the default client to send requests, e.g.:

```rb
zuora = Zuora.default_client

zuora.query("select Id, Name from Product", limit: 1)
```

## Usage

Retrieve a product and its associated product rate plans

```rb
my_product = Zuora::Product.find('product-id-from-zuora')
# => #<Zuora::Product id="product-id-from-zuora" name="My Product Name">

my_product.plans.map(&:name)
# => ["Bronze", "Silver", "Gold", "Platinum"]
```

Find an account bill to email address

```rb
an_account = Zuora::Account.find('account-id-from-zuora')
# => #<Zuora::Account id="account-id-from-zuora" currency="EUR" name="My Account Name">

an_account.bill_to.email
# => "jane.doe@email.com"
```

[create-api-user]: https://knowledgecenter.zuora.com/CF_Users_and_Administrators/A_Administrator_Settings/Manage_Users/Create_an_API_User
