# frozen_string_literal: true

RSpec.describe Zuora do
  it 'has a version number' do
    expect(Zuora::VERSION).not_to be nil
  end
end
